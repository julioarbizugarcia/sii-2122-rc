// Autora: Raquel Cedazo
#ifndef _INC_DADO_
#define _INC_DADO_

class Dado 
{
public:
	void lanzar();

	int getValorCara();

private:
	int valorCara;

};

#endif 

