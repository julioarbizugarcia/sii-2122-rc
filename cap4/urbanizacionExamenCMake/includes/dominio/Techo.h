// Techo.h: interface for the Techo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TECHO_H__2A505376_259E_4F04_BD20_9138CE9DBF22__INCLUDED_)
#define AFX_TECHO_H__2A505376_259E_4F04_BD20_9138CE9DBF22__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class Techo  
{
	float x,y,z;
	float base,altura;

public:
	Techo();
	virtual ~Techo();
	void dibuja();
	void setBase(float ancho) {base=ancho;}
	void setAltura(float alto) {altura=alto;}
	void setPosicion(float ax,float ay, float az)
	{x=ax; y = ay; z =az;}


};

#endif // !defined(AFX_TECHO_H__2A505376_259E_4F04_BD20_9138CE9DBF22__INCLUDED_)
