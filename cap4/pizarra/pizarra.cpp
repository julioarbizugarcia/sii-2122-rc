#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
#define PI 3.141516f
typedef enum{CIRCULO, RECTANGULO} TipoFigura;
class Figura {
protected:
 TipoFigura elTipo; float area;
public:
 float getArea() {return area;}
 TipoFigura getTipo() {return elTipo;}
 static Figura *Factoria(TipoFigura, float, float);
};
class Circulo : public Figura {
 friend class Figura; float radio;
 Circulo(float param):radio(param)
   {elTipo=CIRCULO;area=PI*radio*radio;}
public:
friend ostream& 
operator<<(ostream& os, const Circulo * p){
 return os << "Circulo de radio " << p->radio << endl;
   }
};
class Rectangulo : public Figura {
friend class Figura; float ancho, alto;
Rectangulo(float param1, float param2) 	:ancho(param1),alto(param2) 
  {elTipo=RECTANGULO;area=ancho*alto;}
public:
friend ostream& 
operator<<(ostream& os, const Rectangulo * p) {
return os << "Rectangulo de ancho: " << p->ancho << " y alto: "<< p->alto << endl;
}};


void visualizar(Figura *);
class Pizarra{
 vector<Figura *> listaFiguras;
public:
 void add_Figuras(Figura *p){
   listaFiguras.push_back(p);
  }
 float sumar_areas(){
  float area = 0;
   for(int i=0;i<listaFiguras.size();i++)
	area+=listaFiguras[i]->getArea();
	return (area);
    }
void list(){
for_each(listaFiguras.begin(),listaFiguras.end(),
  visualizar);
}
~Pizarra(){
  for(int i=0;i<listaFiguras.size();i++)
	delete listaFiguras[i];
 }
};

Figura * Figura::Factoria(TipoFigura elTipo, 
 float param1, float param2 = 0){
 if(elTipo == CIRCULO) return new Circulo(param1);
 else return new Rectangulo(param1,param2);
}

void visualizar(Figura *p)
{
 if(p->getTipo() == CIRCULO) cout << (Circulo *)p;
 else cout << (Rectangulo *)p;
}

int main()
{
 Pizarra pizarra;
 pizarra.add_Figuras(Figura::Factoria(CIRCULO,1));
 pizarra.add_Figuras(Figura::Factoria(RECTANGULO,1,2));
 
 cout << "Lista de figuras: "<< endl;
 pizarra.list();
 cout<< "Suma total area: " <<pizarra.sumar_areas()<<endl;
 return 0;
}
